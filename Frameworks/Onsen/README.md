# Onsen.io

[Onsen.io website](https://onsen.io/)

- Programming languages:
    - **Javascript**
- Used technologies:
    - Cordova
    - **Vue, React, Angular**
- Platform development:
    - Android (with cordova)
    - iOS (with cordova)
    - **Web** (PWA)
    - Desktop (with Electron or Node WebKit)
- Initial release:
    - v1.3.15 released on date **19/01/2016**
- Stable release:
    - v.2.10.9 release on date **26/07/2019**
- Current state:
    - 23/09/2020: **active**
- Community level:
    - **2**
    - [Monaca x Onsen community](https://community.onsen.io/)
- Documentation level:
    - **4**
    - [Onsen.io documentation](https://onsen.io/v2/guide/)
- Performance level:
    - TODO
- Common problems
    - Some input bugs
    - [github issues](https://github.com/OnsenUI/OnsenUI/issues?q=is%3Aissue+is%3Aopen)
- Example codes:
    - [Basic Onsen UI examples](https://onsen.io/samples/)
- License
    - Apache license 2.0
    - [Apache License 2.0 github file](https://github.com/OnsenUI/OnsenUI/blob/master/LICENSE)

---

## Code reusability

## Customization


---

- ## Strong points
    1. TODO
- ## Weak points
    1. TODO
---

### Products using this framework

#### Personal considerations
