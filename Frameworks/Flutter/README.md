# Flutter

[Flutter website](https://flutter.dev/)

- Programming languages:
    - **Dart**
- Used technologies:
    - **Flutter SDK**
    - **snapd**
- Platform development:
    - **Android**
    - **iOS**
    - **Web**
    - **macOS**
    - **Linux**
    - Windows (WIP, not stable)
- Initial release:
    - v0.0.6 released on date **12/05/2017**
- Stable release:
    - v.1.20.3 release on date **3/09/2020**
- Current state:
    - 16/09/2020: **active**
- Community level:
    - **5**
    - [Flutter communities](https://flutter.dev/community)
- Documentation level:
    - **5**
    - [Flutter Docs](https://flutter.dev/docs)
- Performance level:
    - TODO
- Common problems
    - TODO
    - [github issues](https://github.com/flutter/flutter/issues)
- Example codes:
    - [iampawan examples repository](https://github.com/iampawan/FlutterExampleApps)
- License
    - BSD v3 license
    - [BSD License github file](https://github.com/flutter/flutter/blob/master/LICENSE)

---

## Code reusability

## Customization


---

- ## Strong points
    1. TODO
- ## Weak points
    1. TODO
---

### Products using this framework

#### Personal considerations
