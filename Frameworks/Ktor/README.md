# Ktor

> Microframework based on Kotlin

[Ktor website](https://ktor.io/)

- Programming languages:
    - **Kotlin**
- Used technologies:
    - **Maven**, **Gradle**
- Platform development:
    - Android (with cordova)
    - iOS (with cordova)
    - **Web** (PWA)
    - Desktop (with Electron or Node WebKit)
- Initial release:
    - v.0.1.2 released on date **11/11/2015**
- Stable release:
    - v.1.4.0 release on date **19/08/2020**
- Current state:
    - 23/09/2020: **active**
- Community level:
    - **2**
    - [Ktor subreddit](https://www.reddit.com/r/ktor/)
    - [Jetbrains Ktor posts](https://blog.jetbrains.com/ktor/)
- Documentation level:
    - **3**
    - [Ktor documentation](https://ktor.io/docs/quickstart-index.html)
    - [Ktor API reference](https://api.ktor.io/?_ga=2.159191288.397893264.1600864901-2140948055.1600071237)
- Performance level:
    - TODO
- Common problems
    - Model and Controller bugs
    - [github issues](https://github.com/ktorio/ktor/issues)
- Example codes:
    - [Inside-repo examples](https://ktor.io/docs/samples-index.html)
- License
    - Apache license 2.0
    - [Apache License 2.0 github file](https://github.com/ktorio/ktor/blob/master/LICENSE)

---

## Code reusability

## Customization


---

- ## Strong points
    1. TODO
- ## Weak points
    1. TODO
---

### Products using this framework

#### Personal considerations
