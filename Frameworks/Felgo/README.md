# Felgo

[Felgo website](https://felgo.com/)

- Programming languages:
    - **QML**, **C++**, **Javascript**
- Used technologies:
    - **QT framework**
- Platform development:
    - **Android**
    - **iOS**
    - **Web**
    - **Desktops**
    - **Embedded**
- Initial release:
    - v.1.0.0 released on date **12/12/2012**
- Stable release:
    - v.3.5.0 release on date **22/04/2020**
- Current state:
    - 23/09/2020: **active**
- Community level:
    - **4**
    - [Felgo forums](https://felgo.com/developers/forums)
    - [Felgo blog](https://blog.felgo.com/)
- Documentation level:
    - **4**
    - [Felgo docs](https://felgo.com/doc/)
- Performance level:
    - TODO
- Common problems
    - Some platform dependant bugs
    - [Felgo forum "bug" search](https://felgo.com/developers/forums/search/bug)
- Example codes:
    - [Felgo showcase app](https://felgo.com/resources/developer-app)
- License
    - Freemium license with limited features
    - [Feature/Cost comparison chart](https://felgo.com/pricing)

---

## Code reusability

## Customization


---

- ## Strong points
    1. TODO
- ## Weak points
    1. TODO
---

### Products using this framework

#### Personal considerations
