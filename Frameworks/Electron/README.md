# Electron

> AKA Atom Shell

[Electron website](https://www.electronjs.org/)

- Programming languages:
    - **Javascript**
    - NodeJS
- Used technologies:
    - Chromium
- Platform development:
    - **Only desktops** (Reported at: [issue 562](https://github.com/electron/electron/issues/562) )
- Initial release:
    - v0.1.0 released on date **15/07/2013**
- Stable release:
    - v.10.1.2 release on date **14/09/2020**
- Current state:
    - 23/09/2020: **active**
- Community level:
    - **5**
    - [Github electron group](https://github.com/electron)
    - [Electron community](https://www.electronjs.org/community)
- Documentation level:
    - **5**
    - [Electron documentation](https://www.electronjs.org/docs)
- Performance level:
    - TODO
- Common problems
    - Overhead due Chromium dependency
    - Some Chromium related bugs
    - [github issues](https://github.com/electron/electron/labels/bug%20%3Abeetle%3A)
- Example codes:
    - [Electron apps library](https://www.electronjs.org/apps)
- License
    - MIT license
    - [MIT License github file](https://github.com/electron/electron/blob/master/LICENSE)

---

## Code reusability

## Customization


---

- ## Strong points
    1. TODO
- ## Weak points
    1. TODO
---

### Products using this framework

#### Personal considerations
