# Phoenix

[Phoenix website](https://www.phoenixframework.org/)

- Programming languages:
    - **Elixir**
- Used technologies:
    - **Elixir**
    - Javascript
    - Cowboy Framework (Erlang)
- Platform development:
    - **Android**
    - **iOS**
    - **Web**
- Initial release:
    - v.0.5.0 released on date **28/08/2015**
- Stable release:
    - v.1.5.4 release on date **21/07/2020**
- Current state:
    - 23/09/2020: **active**
- Community level:
    - **3**
    - [Phoenix blog](https://www.phoenixframework.org/blog)
    - [Elixir language forum](https://elixirforum.com/c/phoenix-forum/20)
- Documentation level:
    - **3**
    - [Phoenix generated documentation](https://hexdocs.pm/phoenix/up_and_running.html)
- Performance level:
    - TODO
- Common problems
    - Kinda new framework, not much used
    - [github issues](https://github.com/phoenixframework/phoenix/issues)
- Example codes:
    - [Hello world webpage](https://hexdocs.pm/phoenix/up_and_running.html#content)
- License
    - MIT license
    - [MIT license github file](https://github.com/phoenixframework/phoenix/blob/master/LICENSE.md)

---

## Code reusability

## Customization


---

- ## Strong points
    1. TODO
- ## Weak points
    1. TODO
---

### Products using this framework

#### Personal considerations
