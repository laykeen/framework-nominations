# Ruby on Rails

[Ruby on Rails website](https://rubyonrails.org/)

- Programming languages:
    - **Ruby**
    - Javascript
- Used technologies:
    - **Ruby**
- Platform development:
    - **Web**, PWA supported
- Initial release:
    - v.1.0 released on date **01/08/2004**
- Stable release:
    - v.6.0.3.2 release on date **17/07/2020**
- Current state:
    - 23/09/2020: **active**
- Community level:
    - **4**
    - [Ruby on rails web blog](https://weblog.rubyonrails.org/)
    - [Ruby on rails forum](https://discuss.rubyonrails.org/)
- Documentation level:
    - **4**
    - [Ruby on rails guide](https://guides.rubyonrails.org/)
- Performance level:
    - TODO
- Common problems
    - Cryptic errors
    - [github issues](https://github.com/rails/rails/issues)
- Example codes:
    - [Ruby on rails guide's example](https://guides.rubyonrails.org/getting_started.html)
- License
    - MIT license
    - [MIT License github file](https://github.com/rails/rails/blob/master/MIT-LICENSE)

---

## Code reusability

## Customization


---

- ## Strong points
    1. TODO
- ## Weak points
    1. TODO
---

### Products using this framework

#### Personal considerations
