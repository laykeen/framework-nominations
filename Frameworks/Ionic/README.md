# Ionic

[Ionic website](https://ionicframework.com/)

- Programming languages:
    - **Javascript**
    - NodeJS
- Used technologies:
    - Cordova, Capacitor, Electron
    - **Vue, React, Angular**
- Platform development:
    - **Android**
    - **iOS**
    - **Web**, PWA ( with [this post](https://dev.to/jankapunkt/transform-any-meteor-app-into-a-pwa-4k44) )
    - **Desktop** (with Electron)
- Initial release:
    - v1.0 released on date **/03/2014**
- Stable release:
    - v.1.10 release on date **26/03/2020**
- Current state:
    - 23/09/2020: **active**
- Community level:
    - **5**
    - [Ionic framework resources](https://ionicframework.com/docs/v3/developer-resources/#)
    - [Ionic github](https://github.com/ionic-team)
- Documentation level:
    - **5**
    - [Ionic forum](https://forums.meteor.com/)
    - [Ionic youtube channel](https://www.youtube.com/channel/UChYheBnVeCfhCmqZfCUdJQw)
- Performance level:
    - TODO (similar to flutter with some native stuff)
- Common problems
    - Some graphical bugs with ionic default components (with and without other ui frameworks)
    - [github issues](https://github.com/ionic-team/ionic-framework/issues?q=is%3Aopen+is%3Aissue+label%3A%22type%3A+bug%22)
- Example codes:
    - [Conference app with angular](https://github.com/ionic-team/ionic-conference-app)
- License
    - Apache license 2.0
    - FOSS, priced cloud hosting options with [Ionic Cloud](https://ionicframework.com/pricing/compare)
    - [Apache License 2.0 github file](https://github.com/ionic-team/ionic-conference-app/blob/master/LICENSE)

---

## Code reusability

## Customization


---

- ## Strong points
    1. TODO
- ## Weak points
    1. TODO
---

### Products using this framework

#### Personal considerations
