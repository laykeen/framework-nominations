# Nativescript

[Nativescript website](https://nativescript.org/)

- Programming languages:
    - **Javascript**
    - Typescript
- Used technologies:
    - **Vue, Angular**
- Platform development:
    - **Android**
    - **iOS**
    - Web
- Initial release:
    - v1.0.0 released on date **08/05/2015**
- Stable release:
    - v.6.3.2 release on date **20/12/2019**
- Current state:
    - 23/09/2020: **active**
- Community level:
    - **3**
    - [Progress software and community](https://nativescript.org/community/)
- Documentation level:
    - **4**
    - [NativeScript documentation](https://docs.nativescript.org/)
    - [NativeScript blog](https://nativescript.org/blog/)
- Performance level:
    - TODO
- Common problems
    - Some platform-related bugs
    - [github issues](https://github.com/NativeScript/NativeScript/issues?q=is%3Aissue+is%3Aopen)
- Example codes:
    - [Test app with NativeScript UI](https://nativescript.org/ui-for-nativescript/#test-drive)
- License
    - Apache license 2.0
    - [Apache License 2.0 github file](https://github.com/NativeScript/NativeScript/blob/master/LICENSE)

---

## Code reusability

## Customization


---

- ## Strong points
    1. Cross-platform app framework
- ## Weak points
    1. TODO
---

### Products using this framework

#### Personal considerations
