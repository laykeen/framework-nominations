# Xamarin

[Xamarin website](https://dotnet.microsoft.com/apps/xamarin)

- Programming languages:
    - **.NET**, **C#**
- Used technologies:
    - Visual Studio
    - **Blazor**, WebAssembly
    - Syncfusion, Telerik, DevExpress, UX Divers, GrapeCity
- Platform development:
    - **Android**
    - **iOS**
    - **Windows**
    - **MacOS**
- Initial release:
    - v0.1.1 released on date **12/12/2012**
- Stable release:
    - v.1.10 release on date **18/03/2020**
- Current state:
    - 23/09/2020: **active**
- Community level:
    - **3**
    - [Microsoft xamarin website](https://dotnet.microsoft.com/apps/xamarin)
    - [Xamarin github](https://github.com/xamarin)
- Documentation level:
    - **4**
    - [Xamarin docs](https://docs.microsoft.com/it-it/xamarin/)
- Performance level:
    - TODO
- Common problems
    - Various problems on View part of MVC
    - [github Xamarin.Forms issues](https://github.com/xamarin/Xamarin.Forms/issues?q=is%3Aissue+is%3Aopen+label%3A%22t%2Fbug+%3Abug%3A%22)
- Example codes:
    - [Xamarin example codes](https://docs.microsoft.com/it-it/samples/browse/?products=xamarin)
- License
    - MIT license
    - [MIT License Xamarin.Forms github file](https://github.com/xamarin/Xamarin.Forms/blob/5.0.0/LICENSE)

---

## Code reusability

## Customization


---

- ## Strong points
    1. TODO
- ## Weak points
    1. TODO
---

### Products using this framework

#### Personal considerations
