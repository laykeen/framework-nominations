# Meteor

[Meteor website](https://www.meteor.com/)

- Programming languages:
    - **Javascript**
    - NodeJS
- Used technologies:
    - MongoDB
    - **Vue, React, Svelte, Angular, Blaze**
- Platform development:
    - **Android**
    - **iOS**
    - **Web**, PWA ( with [this post](https://dev.to/jankapunkt/transform-any-meteor-app-into-a-pwa-4k44) )
- Initial release:
    - v0.1.1 released on date **20/01/2012**
- Stable release:
    - v.1.10 release on date **18/03/2020**
- Current state:
    - 15/09/2020: **active**
- Community level:
    - **4**
    - [Meteor Software company](https://www.meteor.com/culture)
- Documentation level:
    - **5**
    - [Meteor forum](https://forums.meteor.com/)
    - [Meteor guide](https://guide.meteor.com/)
- Performance level:
    - TODO
- Common problems
    - Some npm releated bugs as usual
    - [github issues](https://github.com/meteor/meteor/issues?q=is%3Aopen+is%3Aissue)
- Example codes:
    - [Whatsapp clone with angular](https://angular-meteor.com/tutorials/whatsapp/meteor/bootstrapping)
- License
    - MIT license
    - FOSS, priced cloud hosting options with [Meteor Galaxy](https://www.meteor.com/hosting#pricing)
    - [MIT License github file](https://github.com/meteor/meteor/blob/devel/LICENSE)

---

## Code reusability

## Customization


---

- ## Strong points
    1. Cross-platform app framework
- ## Weak points
    1. TODO
---

### Products using this framework

#### Personal considerations
