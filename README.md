# Framework Nominations

Here be frameworks

## Base code used to evaluate frameworks

You can find the code concept [here](Base_code/README.md).

## Nominee rules

1. Every framework should be as much cross-platform as possible
1. Every framework has to be in a stable state
1. Every framework has to be _alive_
1. Every framework should be well documented

## Valutation parameters

| Parameter | Explanation |
|:---:|:---|
| Programming languages | List of supported programming languages used to develop the software using that framework. <br/><br/> _Example_: `C++`,`Java`,`C#`,` Python` |
| Used technologies | List of libraries, tools, direct dipendences required or suggested by the framework. Those can be installed following each framework README instructions. <br/><br/> _Example_: `php7.1`,`libssl`,`Android Studio v4.0.1` |
| Platform development | Where the framework can run and compile. <br/><br/> _Example_: `Windows 10 v.1903`, `Debian 10.5` |
| Platform output | List of platforms that the framework supports. <br/><br/> _Example_: `Android 10`, `iOS 13.6` |
| Initial release | Date of the first official AND stable release. <br/><br/> _Example_: `v1.0 released on date 15/10/2015` |
| Stable release | Date of the latest stable offficial release <br/><br/> _Example_: `v5.0 released on date 15/10/2019` |
| Current state | One of the possible state of the project: <br/> **active** <br/> **paused** <br/> **inactive** <br/> **dead** <br/><br/> _Example_: `14/09/2020: active` |
| Community level | How much is present the community for the project: <br/> **5**. The project is the community: The framework is made by the community itself and has tons of members <br/> **4**. Active community: Multiple forums, +1k stack overflow discussions, global meetings <br/> **3**. Corporation and its community: The framework is made by a company and has a medium/small community that has to follow a procedure to submit some modifications <br/> **2**. Small community: The framework is new or it hadn't a good marketing around it <br/> **1**. Still active but not so used: The framework is still alive but it is going to EOL <br/> **0**. Zero community: Usually when the project and its code is unmaintained for years. <br/><br/> _Example_: `4` |
| Documentation level | How effective is the documentation for development: <br/> **5**. Wikipedia mode: The framework uses a wiki-like software with explained examples for base and advanced cases <br/> **4**. Handwritten documentation: Well tested documentation made by framework developers, but also has some examples. <br/> **3**. Generated documentation: The framework uses tools like javadoc for automatic documentation of functions, variables, classes, ecc... <br/> **2**. Generic examples with comments: Some open source repositories used as official examples with the framework <br/> **1**. Some blogs talked about it: no real official documentation, just some links that talks/use the framework. <br/> **0**. Just the README: usually when the framework born yesterday. <br/><br/> _Example_: `4` |
| Performance level | Simple report about the framework: <br/> **Compile time**: time reported by the framework itself or by some generic command execution timer <br/> **Test execution time**: First Contentful Paint, Speed Index, Largest Contentful Paint, Time To Interactive, Total Blocking time, Cumulative Layout Shift. (some frameworks output only FCI) <br/> **Production execution time**: same tests used in _Test execution time_. <br/><br/> _Example_: `Compile time: 340ms`, `Test execution time: FCP: 58ms, TTI: 90ms`, `Production execution time: FCP: 38ms, TTI: 50ms` |
| Common problems | Short list of mayor problems found on StackOverflow and/or git Issues with links. <br/><br/> _Example_: `Resize window crash on Windows platforms` [Link to issue](#)  |
| Example codes | Link to the repository containing the example code, where in the README you can find installation instructions, references and guides. <br/><br/> _Example_: `Repository name` [Link to repo](#) |
| License | Type of license used in the project, a link to it and if it can be used for commercial porpouses. <br/><br/> _Example_: `MIT license` [Link to license](#)|

---

## Candidates

[Candidates list](Candidates.md): here you can find all the proposals with the following format:

1. [Framework report template](#)
    - [report template README](#)
    - [report template example](#)
    - Status:
        1. [ ] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report
1. ...


---

### License

This entire project is under MIT license:

Copyright 2020 [@nicola.basso](https://gitlab.com/nicola.basso)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#### Disclaimer

This project and every document inside it may not be updated with the latest data (started 14/09/2020 ended TODO).
Also performance reports may vary, but that's kinda obvious.
